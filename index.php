<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            tt strong:before{
                content:"";
                display:table;
            }
            </style>


        <title>Radio</title>

        <script type="text/javascript" src="bower_components/jquery/jquery.js"></script>
        <script type="text/javascript" >
            $(document).ready(function() {
                var theDiv = '#refresh';
                var theCounter = '#timeout';
                var timer = null;
                var interval = 30 * 1000; //30 sec.
                var countDown = interval;
                (function refreshDiv() {
                     $(theCounter).html('Updating...');
                    window.clearInterval(timer);
                    countDown = interval;

                    $.get('status.php?' + Math.random(), function(html) {
                        timer = window.setInterval(function() {
                            countDown -= 1000;
                            $(theCounter).html(parseInt(countDown / 1000, 10));
                        }, 1000);
                        //tot tabelu
//                        $(theDiv).html(html); 

                        //numa lista
                        $(theDiv).html('');
                        $(html).find('tt').appendTo($(theDiv)); 
                        window.setTimeout(refreshDiv, interval);
                    });
                })();
            });

        </script>
    </head>
    <body>
        <h1>
            Watch me: <span id="timeout"></span>
        </h1>
        <div id="refresh">
        </div>

    </body>
</html>